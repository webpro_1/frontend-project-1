import http from "./axios";

const login = (username: string, password: string) => {
  return http.post("/auth/login", { username, password });
};

export default {login};
