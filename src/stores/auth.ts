import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const authName = ref("");

  const login = async (username: string, password: string) => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("user", JSON.stringify(res.data.user));
    } catch (e) {
      messageStore.showError("Username or password is incorrect");
    }
    loadingStore.isLoading = false;
    router.push("/");
  };
  const logout = () => {
    loadingStore.isLoading = true;
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    loadingStore.isLoading = false;
    router.replace("/login");
  };

  return { authName, login, logout };
});
